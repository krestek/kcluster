exports.isMaster = () => {
    const cluster = require('cluster');
    if (cluster.isMaster) {
        var cpuCount = require('os').cpus().length;
        for (var i = 0; i < cpuCount; i += 1) {
            var b = i;
            var worker = cluster.fork();
            worker.cluster = {
                worker: {
                    id: b
                }
            }
            worker.on('message', msg => {
                for (var id in cluster.workers) {
                    cluster.workers[id].send(msg);
                }
            })
        }
        cluster.on('exit', function (worker) {
            console.log('Worker %d died :(', worker.id);
            cluster.fork();
        });
        return true;
    }
    global.workerid = cluster.worker.id;
    process.on('message', msg => {
        try {
            global.onMsg(msg);
        } catch (err) {
            console.log("kcluster error: global.rec is undefined.");
        }
    })
}