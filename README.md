# kcluster - Clustering made simple
## Features:
-Automate clustering with simple 2 lines.

-Easy communication between workers, it will send process.send() to all workers.

-Simplified message receive action.

## Tutorial:
1. In the command line, type: ```npm i kcluster```
2. In your starting file (index.js by default) add this code at the very top:

```js
if (require('kluster').isMaster())
    return;
```
That's it, your code will be forked to the amount of threads your cpu has.

## Further Info:
**-You can read worker id using:**

```js
global.workerid
```

**-Here's a sample for sending a message to all workers and calling a function on receive on every worker:**

```js
if (require('kcluster').isMaster())
    return;

//Sending a message to all workers:
if (global.workerid == 1) //So only worker 1 will send it
    process.send({ item1: 'example', item2: 'example' });

//Fuction to call when receiving the message: (It will run on every worker)
global.onMsg = msg => {
    console.log("Worker " + global.workerid + " received message: " + msg.item1);
}
//If you have 4 threads CPU, the output will be:
/*
    Worker 1 received message: example
    Worker 2 received message: example
    Worker 3 received message: example
    Worker 4 received message: example
*/
```



